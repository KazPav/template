package com.training.registry.registryservice.api.dto;


import java.util.Objects;

public class DeviceDto {
    private String vendor;
    private String model;
    private String serNum;
    private String mac;
    private String ip;
    private String netmask;

    public DeviceDto() {
    }

    public DeviceDto(String vendor, String model, String serNum, String mac) {
        this.vendor = vendor;
        this.model = model;
        this.serNum = serNum;
        this.mac = mac;
    }

    public DeviceDto(String vendor, String model, String serNum, String mac, String ip, String netmask) {
        this.vendor = vendor;
        this.model = model;
        this.serNum = serNum;
        this.mac = mac;
        this.ip = ip;
        this.netmask = netmask;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSerNum() {
        return serNum;
    }

    public void setSerNum(String serNum) {
        this.serNum = serNum;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getNetmask() {
        return netmask;
    }

    public void setNetmask(String netmask) {
        this.netmask = netmask;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeviceDto deviceDto = (DeviceDto) o;
        return Objects.equals(vendor, deviceDto.vendor) &&
                Objects.equals(model, deviceDto.model) &&
                Objects.equals(serNum, deviceDto.serNum) &&
                Objects.equals(mac, deviceDto.mac) &&
                Objects.equals(ip, deviceDto.ip) &&
                Objects.equals(netmask, deviceDto.netmask);
    }

    @Override
    public int hashCode() {
        return Objects.hash(vendor, model, serNum, mac, ip, netmask);
    }
}
