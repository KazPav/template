package com.training.registry.registryservice.service.impl;

import com.training.registry.registryservice.api.dto.AddDeviceRequestDto;
import com.training.registry.registryservice.api.dto.ConfigurationDto;
import com.training.registry.registryservice.api.dto.DeviceDto;
import com.training.registry.registryservice.exception.NotFoundException;
import com.training.registry.registryservice.model.Device;
import com.training.registry.registryservice.repository.DeviceRepository;
import com.training.registry.registryservice.repository.DeviceRepositoryCustom;
import com.training.registry.registryservice.service.DeviceService;
import com.training.registry.registryservice.service.RestTemaplateService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DeviceServiceImpl implements DeviceService {
    private DeviceRepository deviceRepository;

    private ModelMapper modelMapper;

    private MongoTemplate mongoTemplate;

    private RestTemaplateService restTemaplateService;

    private DeviceRepositoryCustom deviceRepositoryCustom;

    @Override
    public void addDevice(AddDeviceRequestDto addDeviceRequestDto) {
        ConfigurationDto configBySerNum = restTemaplateService.getConfigBySerNum(addDeviceRequestDto.getSerNum());
        Device newDevice = modelMapper.map(addDeviceRequestDto, Device.class);
        newDevice.setIp(configBySerNum.getIp());
        newDevice.setNetmask(configBySerNum.getNetmask());
        deviceRepository.save(newDevice);
    }

    @Override
    public DeviceDto getDeviceBySerNum(String serNum) {
        Optional<Device> optionalDevice = deviceRepository.findById(serNum);
        return modelMapper.map(optionalDevice.orElseThrow(() -> new NotFoundException(Device.class, "serNum", serNum)),
                DeviceDto.class);
    }

    @Override
    public Page<DeviceDto> getDevicesByParams(int page, int size, String vendor, String model) {
        Page<Device> devicePage = deviceRepositoryCustom.getDevicesByParams(page, size, vendor, model);
        return devicePage.map(device -> modelMapper
                .map(device, DeviceDto.class));
    }

    @Autowired
    public void setDeviceRepository(DeviceRepository deviceRepository) {
        this.deviceRepository = deviceRepository;
    }

    @Autowired
    public void setModelMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Autowired
    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Autowired
    public void setRestTemaplateService(RestTemaplateService restTemaplateService) {
        this.restTemaplateService = restTemaplateService;
    }

    @Autowired
    public void setDeviceRepositoryCustom(DeviceRepositoryCustom deviceRepositoryCustom) {
        this.deviceRepositoryCustom = deviceRepositoryCustom;
    }
}
