package com.training.registry.registryservice.service;

import com.training.registry.registryservice.api.dto.ConfigurationDto;

public interface RestTemaplateService {
    ConfigurationDto getConfigBySerNum(String serNum);
}
