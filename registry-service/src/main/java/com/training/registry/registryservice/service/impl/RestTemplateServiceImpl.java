package com.training.registry.registryservice.service.impl;

import com.training.registry.registryservice.api.dto.ConfigurationDto;
import com.training.registry.registryservice.exception.NotFoundException;
import com.training.registry.registryservice.service.RestTemaplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service
public class RestTemplateServiceImpl implements RestTemaplateService {

    private RestTemplate restTemplate;

    @Value("${configuration.service.url}")
    private String configServiceUrl;

    @Override
    public ConfigurationDto getConfigBySerNum(String serNum) {
        ConfigurationDto configurationDto = null;
        try {
            configurationDto = restTemplate
                    .getForObject(configServiceUrl + "/configuration/" + serNum, ConfigurationDto.class);
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
                throw new NotFoundException(ConfigurationDto.class, "serNum", serNum);
            }
        }
        return configurationDto;

    }

    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
}
