package com.training.registry.registryservice.api.dto;

import java.io.Serializable;

public class ConfigurationDto implements Serializable {
    private String serNum;
    private String ip;
    private String netmask;

    public ConfigurationDto() {
    }

    public ConfigurationDto(String serNum, String ip, String netmask) {
        this.serNum = serNum;
        this.ip = ip;
        this.netmask = netmask;
    }

    public String getSerNum() {
        return serNum;
    }

    public void setSerNum(String serNum) {
        this.serNum = serNum;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getNetmask() {
        return netmask;
    }

    public void setNetmask(String netmask) {
        this.netmask = netmask;
    }
}
