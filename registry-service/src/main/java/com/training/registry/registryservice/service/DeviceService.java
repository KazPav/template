package com.training.registry.registryservice.service;

import com.training.registry.registryservice.api.dto.AddDeviceRequestDto;
import com.training.registry.registryservice.api.dto.DeviceDto;
import org.springframework.data.domain.Page;

public interface DeviceService {
    void addDevice(AddDeviceRequestDto addDeviceRequestDto);

    DeviceDto getDeviceBySerNum(String serNum);

    Page<DeviceDto> getDevicesByParams(int page, int size, String vendor, String model);

}
