package com.training.registry.registryservice.api.dto;

import javax.validation.constraints.NotEmpty;
import java.util.Objects;

public class AddDeviceRequestDto {
    @NotEmpty
    private String vendor;
    @NotEmpty
    private String model;
    @NotEmpty
    private String serNum;
    @NotEmpty
    private String mac;

    public AddDeviceRequestDto() {
    }

    public AddDeviceRequestDto(String vendor, String model, String serNum, String mac) {
        this.vendor = vendor;
        this.model = model;
        this.serNum = serNum;
        this.mac = mac;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSerNum() {
        return serNum;
    }

    public void setSerNum(String serNum) {
        this.serNum = serNum;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddDeviceRequestDto that = (AddDeviceRequestDto) o;
        return Objects.equals(vendor, that.vendor) &&
                Objects.equals(model, that.model) &&
                Objects.equals(serNum, that.serNum) &&
                Objects.equals(mac, that.mac);
    }

    @Override
    public int hashCode() {
        return Objects.hash(vendor, model, serNum, mac);
    }
}
