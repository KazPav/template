package com.training.registry.registryservice.repository.impl;

import com.training.registry.registryservice.model.Device;
import com.training.registry.registryservice.repository.DeviceRepositoryCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DeviceRepositoryImpl implements DeviceRepositoryCustom {

    private MongoTemplate mongoTemplate;

    @Override
    public Page<Device> getDevicesByParams(int page, int size, String vendor, String model) {
        Query query = new Query();
        if (vendor != null && !vendor.equals("")) {
            query.addCriteria(Criteria.where("vendor").is(vendor));
        }
        if (model != null && !vendor.equals("")) {
            query.addCriteria(Criteria.where("model").is(model));
        }
        PageRequest pageRequest = PageRequest.of(page, size);
        query.with(pageRequest);
        List<Device> devices = mongoTemplate.find(query, Device.class);
        return PageableExecutionUtils.getPage(devices,
                pageRequest, () -> mongoTemplate.count(query, Device.class));
    }

    @Autowired
    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }
}
