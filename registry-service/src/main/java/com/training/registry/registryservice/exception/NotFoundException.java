package com.training.registry.registryservice.exception;


public class NotFoundException extends RuntimeException {
    public NotFoundException(Class clazz, String paramName, String paramValue) {
        super("Could not find " + clazz.getSimpleName() + " by parameter '" + paramName + "' and value '" + paramValue + "'");
    }
}
