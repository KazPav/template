package com.training.registry.registryservice.api.controller;

import com.training.registry.registryservice.api.dto.AddDeviceRequestDto;
import com.training.registry.registryservice.api.dto.DeviceDto;
import com.training.registry.registryservice.service.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class DeviceController {

    private DeviceService deviceService;

    @PostMapping("/device")
    public ResponseEntity<?> addDevice(@Valid @RequestBody AddDeviceRequestDto deviceDto) {
        deviceService.addDevice(deviceDto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping("/devices")
    public Page<DeviceDto> getAllDevicesByParams(@RequestParam(value = "page", defaultValue = "0") int page,
                                                 @RequestParam(value = "size", defaultValue = "5") int size,
                                                 @RequestParam(value = "vendor", required = false) String vendor,
                                                 @RequestParam(value = "model", required = false) String model) {
        return deviceService.getDevicesByParams(page, size, vendor, model);
    }

    @GetMapping("/device/{serNum}")
    public DeviceDto getDeviceBySerNum(@PathVariable("serNum") String serNum) {
        return deviceService.getDeviceBySerNum(serNum);
    }

    @Autowired
    public void setDeviceService(DeviceService deviceService) {
        this.deviceService = deviceService;
    }
}
