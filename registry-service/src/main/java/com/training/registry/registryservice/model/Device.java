package com.training.registry.registryservice.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "device")
public class Device {
    private String vendor;
    private String model;
    @Id
    private String serNum;
    private String mac;
    private String ip;
    private String netmask;

    public Device() {
    }

    public Device(String vendor, String model, String serNum, String mac) {
        this.vendor = vendor;
        this.model = model;
        this.serNum = serNum;
        this.mac = mac;
    }

    public Device(String vendor, String model, String serNum, String mac, String ip, String netmask) {
        this.vendor = vendor;
        this.model = model;
        this.serNum = serNum;
        this.mac = mac;
        this.ip = ip;
        this.netmask = netmask;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSerNum() {
        return serNum;
    }

    public void setSerNum(String serNum) {
        this.serNum = serNum;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getNetmask() {
        return netmask;
    }

    public void setNetmask(String netmask) {
        this.netmask = netmask;
    }
}
