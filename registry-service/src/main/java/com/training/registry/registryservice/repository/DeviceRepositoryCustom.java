package com.training.registry.registryservice.repository;

import com.training.registry.registryservice.model.Device;
import org.springframework.data.domain.Page;

public interface DeviceRepositoryCustom {
    Page<Device> getDevicesByParams(int page, int size, String vendor, String model);
}
