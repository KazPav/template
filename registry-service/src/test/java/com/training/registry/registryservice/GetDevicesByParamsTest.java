package com.training.registry.registryservice;

import com.training.registry.registryservice.model.Device;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GetDevicesByParamsTest extends AbstractIntegrationTest {

    private List<Device> testDevices;

    private final static String VENDOR1 = "someVendor1";
    private final static String VENDOR2 = "someVendor2";
    private final static String MODEL1 = "someModel1";
    private final static String MODEL2 = "someModel2";
    private final static String MODEL3 = "someModel3";

    @Before
    public void init() {
        testDevices = deviceRepository.saveAll(List.of(
                new Device(VENDOR1, MODEL1, "01ABCDE01",
                        "00:A0:C9:14:C8:29", "172.16.0.50", "255.255.0.0"),
                new Device(VENDOR1, MODEL1, "02ABCDE02",
                        "00:A0:C9:14:C8:29", "172.16.0.50", "255.255.0.0"),
                new Device(VENDOR2, MODEL2, "03ABCDE03",
                        "00:A0:C9:14:C8:29", "172.16.0.50", "255.255.0.0"),
                new Device(VENDOR2, MODEL2, "04ABCDE04",
                        "00:A0:C9:14:C8:29", "172.16.0.50", "255.255.0.0"),
                new Device(VENDOR2, MODEL3, "05ABCDE05",
                        "00:A0:C9:14:C8:29", "172.16.0.50", "255.255.0.0")));
    }

    @Test
    public void testGetDeviceByParamsSuccessful() throws Exception {
        String urlTemplate = "/devices";

        mockMvc.perform(MockMvcRequestBuilders
                .get(urlTemplate)
                .param("page", "0")
                .param("size", "5"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(testDevices.size())));

        int numberOfDevicesWithVendor1 = (int) testDevices
                .stream()
                .filter(device -> device.getVendor().equals(VENDOR1))
                .count();
        mockMvc.perform(MockMvcRequestBuilders
                .get(urlTemplate)
                .param("page", "0")
                .param("size", "5")
                .param("vendor", VENDOR1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(numberOfDevicesWithVendor1)));

        int numberOfDevicesWithVendor2AndModel2 = (int) testDevices
                .stream()
                .filter(device -> device.getVendor().equals(VENDOR2) && device.getModel().equals(MODEL2))
                .count();
        mockMvc.perform(MockMvcRequestBuilders
                .get(urlTemplate)
                .param("page", "0")
                .param("size", "5")
                .param("vendor", VENDOR2)
                .param("model", MODEL2))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(numberOfDevicesWithVendor2AndModel2)));
    }

    @After
    public void clean(){
        deviceRepository.deleteAll();
    }
}
