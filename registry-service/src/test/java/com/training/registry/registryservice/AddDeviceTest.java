package com.training.registry.registryservice;


import com.training.registry.registryservice.api.dto.AddDeviceRequestDto;
import com.training.registry.registryservice.api.dto.ConfigurationDto;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AddDeviceTest extends AbstractIntegrationTest {

    private AddDeviceRequestDto testDeviceDto;

    @Before
    public void init() {
        testDeviceDto = new AddDeviceRequestDto("SomeVendor", "SomeModel",
                "02ABCDEF02", "00:A0:C9:14:C8:29");
    }

    @Test
    public void addDeviceSuccessful() throws Exception {
        String urlTemplate = "/device";
        Mockito.when(restTemaplateService.getConfigBySerNum(testDeviceDto.getSerNum()))
                .thenReturn(new ConfigurationDto(testDeviceDto.getSerNum(), "172.16.0.50", "255.255.0.0"));

        mockMvc.perform(MockMvcRequestBuilders
                .post(urlTemplate)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(testDeviceDto)))
                .andExpect(status().isCreated());
    }

    @After
    public void clean() {
        deviceRepository.deleteAll();
    }
}
