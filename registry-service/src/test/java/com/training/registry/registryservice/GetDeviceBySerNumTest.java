package com.training.registry.registryservice;

import com.fasterxml.jackson.core.type.TypeReference;
import com.training.registry.registryservice.api.dto.DeviceDto;
import com.training.registry.registryservice.model.Device;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GetDeviceBySerNumTest extends AbstractIntegrationTest {

    private Device testDevice;

    @Before
    public void init() {
        testDevice = deviceRepository.save(
                new Device("someVendor1", "someModel1",
                        "123456", "00:A0:C9:14:C8:29", "172.16.0.50", "255.255.0.0"));
    }

    @Test
    public void getDevicesBySerNumSuccessful() throws Exception {
        String urlTemplate = "/device/" + testDevice.getSerNum();
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders
                .get(urlTemplate))
                .andExpect(status().isOk())
                .andReturn();
        DeviceDto deviceDto = objectMapper.readValue(mvcResult.getResponse().getContentAsString(),
                new TypeReference<DeviceDto>() {
                });
        DeviceDto expectedDeviceDto = modelMapper.map(testDevice, DeviceDto.class);
        assertEquals(deviceDto, expectedDeviceDto);
    }

    @After
    public void clean() {
        deviceRepository.deleteAll();
    }
}
