package com.training.registry.registryservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.training.registry.registryservice.repository.DeviceRepository;
import com.training.registry.registryservice.service.RestTemaplateService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@AutoConfigureMockMvc
public class AbstractIntegrationTest {

    @MockBean
    protected RestTemaplateService restTemaplateService;

    @Autowired
    protected ObjectMapper objectMapper;

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected ModelMapper modelMapper;

    @Autowired
    protected DeviceRepository deviceRepository;
}
