package com.training.configuration.configurationservice;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.training.configuration.configurationservice.api.dto.ConfigurationDto;
import com.training.configuration.configurationservice.model.Configuration;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Set;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest
public class GetAllConfigurationsTest extends AbstractIntegrationTest {

    private Set<Configuration> configurations;

    @Before
    public void init() {
        configurations = Set.of(
                new Configuration("03ABCDEF03", "172.16.0.50", "255.255.0.0"),
                new Configuration("04ABCDEF04", "192.168.0.101", "255.255.255.0"));
        configurationRepository.saveAll(configurations);
    }

    @Test
    public void getAllConfigurationsSuccessful() throws Exception {
        String getAllConfigsUrlTemplate = "/configurations";
        mockMvc.perform(MockMvcRequestBuilders
                .get(getAllConfigsUrlTemplate)
                .param("page", "0")
                .param("size", "5"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(2)));
    }

    @After
    public void clean() {
        configurationRepository.deleteAll();
    }
}
