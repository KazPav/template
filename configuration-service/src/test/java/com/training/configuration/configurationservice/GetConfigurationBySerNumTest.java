package com.training.configuration.configurationservice;

import com.fasterxml.jackson.core.type.TypeReference;
import com.training.configuration.configurationservice.api.dto.ConfigurationDto;
import com.training.configuration.configurationservice.model.Configuration;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest
public class GetConfigurationBySerNumTest extends AbstractIntegrationTest {

    private Configuration testConfiguration;
    public static final String URL_TEMPLATE = "/configuration/";

    @Before
    public void init() {
        testConfiguration = configurationRepository.save(
                new Configuration("01ABCDEF01", "192.168.0.101", "255.255.255.0"));
    }

    @Test
    public void getConfigurationBySerNumSuccessful() throws Exception {
        String currentUrlTemplate = URL_TEMPLATE + testConfiguration.getSerNum();
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders
                .get(currentUrlTemplate))
                .andExpect(status().isOk())
                .andReturn();
        ConfigurationDto responseConfiguration = objectMapper.readValue(mvcResult.getResponse().getContentAsString(),
                new TypeReference<ConfigurationDto>() {
                });
        ConfigurationDto expectedConfigurationDto = modelMapper.map(testConfiguration, ConfigurationDto.class);
        assertEquals(expectedConfigurationDto, responseConfiguration);
    }

    @Test
    public void getConfigurationByNonExistingSerNumSuccessful() throws Exception {
        String currentUrlTemplate = URL_TEMPLATE + "notExistingSerNum";
        mockMvc.perform(MockMvcRequestBuilders
                .get(currentUrlTemplate))
                .andExpect(status().isNotFound());
    }

    @After
    public void clean() {
        configurationRepository.deleteAll();
    }

}
