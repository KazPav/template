package com.training.configuration.configurationservice;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.training.configuration.configurationservice.api.dto.ConfigurationDto;
import com.training.configuration.configurationservice.model.Configuration;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AddConfigurationTest extends AbstractIntegrationTest {

    private static final String URL_TEMPLATE = "/configuration";


    @Test
    public void addConfigurationsSuccessful() throws Exception {
        ConfigurationDto testConfigurationDto = new ConfigurationDto("02ABCDEF02",
                "192.168.0.101", "255.255.255.0");
        mockMvc.perform(MockMvcRequestBuilders
                .post(URL_TEMPLATE)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(testConfigurationDto)))
                .andExpect(status().isCreated());
        Optional<Configuration> configFromDb = configurationRepository.findById(testConfigurationDto.getSerNum());
        assertTrue(configFromDb.isPresent());
        assertEquals(testConfigurationDto.getIp(), configFromDb.get().getIp());
        assertEquals(testConfigurationDto.getNetmask(), configFromDb.get().getNetmask());
    }

    @Test
    public void addConfigurationInvalidDataReturnsBadRequest() throws Exception {
        ConfigurationDto invalidConfigurationDto = new ConfigurationDto("02ABCDEF02",
                "192.168.0.101", "");
        mockMvc.perform(MockMvcRequestBuilders
                .post(URL_TEMPLATE)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(invalidConfigurationDto)))
                .andExpect(status().isBadRequest());

        invalidConfigurationDto = new ConfigurationDto("02ABCDEF02",
                "", "255.255.255.0");
        mockMvc.perform(MockMvcRequestBuilders
                .post(URL_TEMPLATE)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(invalidConfigurationDto)))
                .andExpect(status().isBadRequest());

        invalidConfigurationDto = new ConfigurationDto("",
                "192.168.0.101", "255.255.255.0");
        mockMvc.perform(MockMvcRequestBuilders
                .post(URL_TEMPLATE)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(invalidConfigurationDto)))
                .andExpect(status().isBadRequest());
    }


    @After
    public void clear() {
        configurationRepository.deleteAll();
    }
}
