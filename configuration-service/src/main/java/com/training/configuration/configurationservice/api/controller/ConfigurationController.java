package com.training.configuration.configurationservice.api.controller;

import com.training.configuration.configurationservice.api.dto.ConfigurationDto;
import com.training.configuration.configurationservice.service.ConfigurationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class ConfigurationController {
    private ConfigurationService configurationService;

    @PostMapping("/configuration")
    public ResponseEntity<?> addConfiguration(@Valid @RequestBody ConfigurationDto configurationDto) {
        configurationService.addConfiguration(configurationDto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping("/configurations")
    public Page<ConfigurationDto> getAllConfigurations(@RequestParam(value = "page", defaultValue = "0") int page,
                                                       @RequestParam(value = "size", defaultValue = "5") int size) {
        return configurationService.getAllConfigurations(page, size);
    }

    @GetMapping("/configuration/{serNum}")
    public ConfigurationDto getConfigurationBySerNum(@PathVariable("serNum") String serNum) {
        return configurationService.getConfigurationBySerNum(serNum);
    }

    @Autowired
    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }
}
