package com.training.configuration.configurationservice.api.dto;

import javax.validation.constraints.NotEmpty;
import java.util.Objects;

public class ConfigurationDto {

    @NotEmpty
    private String serNum;
    @NotEmpty
    private String ip;
    @NotEmpty
    private String netmask;

    public ConfigurationDto() {
    }

    public ConfigurationDto(String serNum, String ip, String netmask) {
        this.serNum = serNum;
        this.ip = ip;
        this.netmask = netmask;
    }

    public String getSerNum() {
        return serNum;
    }

    public void setSerNum(String serNum) {
        this.serNum = serNum;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getNetmask() {
        return netmask;
    }

    public void setNetmask(String netmask) {
        this.netmask = netmask;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConfigurationDto that = (ConfigurationDto) o;
        return Objects.equals(serNum, that.serNum) &&
                Objects.equals(ip, that.ip) &&
                Objects.equals(netmask, that.netmask);
    }

    @Override
    public int hashCode() {
        return Objects.hash(serNum, ip, netmask);
    }

    @Override
    public String toString() {
        return "ConfigurationDto{" +
                "serNum='" + serNum + '\'' +
                ", ip='" + ip + '\'' +
                ", netmask='" + netmask + '\'' +
                '}';
    }
}
