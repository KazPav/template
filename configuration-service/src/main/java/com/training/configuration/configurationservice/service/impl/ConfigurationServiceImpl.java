package com.training.configuration.configurationservice.service.impl;

import com.training.configuration.configurationservice.api.dto.ConfigurationDto;
import com.training.configuration.configurationservice.exception.NotFoundException;
import com.training.configuration.configurationservice.model.Configuration;
import com.training.configuration.configurationservice.repository.ConfigurationRepository;
import com.training.configuration.configurationservice.service.ConfigurationService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ConfigurationServiceImpl implements ConfigurationService {

    private ConfigurationRepository configurationRepository;

    private ModelMapper modelMapper;

    @Override
    public void addConfiguration(ConfigurationDto configurationDto) {
        Configuration config = modelMapper.map(configurationDto, Configuration.class);
        configurationRepository.save(config);
    }

    @Override
    public Page<ConfigurationDto> getAllConfigurations(int page, int size) {
        Page<Configuration> configPage = configurationRepository.findAll(PageRequest.of(page, size));
        return configPage.map(configuration -> modelMapper.map(configuration, ConfigurationDto.class));
    }

    @Override
    public ConfigurationDto getConfigurationBySerNum(String serNum) {
        Optional<Configuration> optionalConfiguration = configurationRepository.findById(serNum);
        return modelMapper.map(optionalConfiguration
                        .orElseThrow(() -> new NotFoundException(Configuration.class, "serNum", serNum)),
                ConfigurationDto.class);
    }

    @Autowired
    public void setConfigurationRepository(ConfigurationRepository configurationRepository) {
        this.configurationRepository = configurationRepository;
    }

    @Autowired
    public void setModelMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }
}
