package com.training.configuration.configurationservice.service;

import com.training.configuration.configurationservice.api.dto.ConfigurationDto;
import org.springframework.data.domain.Page;


public interface ConfigurationService {
    void addConfiguration(ConfigurationDto configurationDto);

    Page<ConfigurationDto> getAllConfigurations(int page, int size);

    ConfigurationDto getConfigurationBySerNum(String serNum);
}
