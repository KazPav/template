package com.training.configuration.configurationservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException {
    public NotFoundException(Class clazz, String paramName, String paramValue) {
        super("Could not find " + clazz.getSimpleName() + " by parameter '" + paramName + "' and value '" + paramValue + "'");
    }
}
