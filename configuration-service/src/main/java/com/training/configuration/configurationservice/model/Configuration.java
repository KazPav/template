package com.training.configuration.configurationservice.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document(collection = "configuration")
public class Configuration {
    @Id
    private String serNum;
    private String ip;
    private String netmask;

    public Configuration() {
    }

    public Configuration(String serNum, String ip, String netmask) {
        this.serNum = serNum;
        this.ip = ip;
        this.netmask = netmask;
    }

    public String getSerNum() {
        return serNum;
    }

    public void setSerNum(String serNum) {
        this.serNum = serNum;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getNetmask() {
        return netmask;
    }

    public void setNetmask(String netmask) {
        this.netmask = netmask;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Configuration that = (Configuration) o;
        return Objects.equals(serNum, that.serNum) &&
                Objects.equals(ip, that.ip) &&
                Objects.equals(netmask, that.netmask);
    }

    @Override
    public int hashCode() {
        return Objects.hash(serNum, ip, netmask);
    }

    @Override
    public String toString() {
        return "Configuration{" +
                "serNum='" + serNum + '\'' +
                ", ip='" + ip + '\'' +
                ", netmask='" + netmask + '\'' +
                '}';
    }
}
